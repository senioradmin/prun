# prun - Configuration management with parallel-ssh

prun is shellscript-wrapper for the pssh tools at https://github.com/lilydjwg/pssh. pssh or parallel-ssh (not to be confused the client library of the same name) is a set of tools which allows to execute commands over ssh in parallel on multiple hosts.

This wrapper closes the gap needed to turn pssh into a simple configuration management tool. In essence, prun + pssh can be used as a replacement for Ansible, Puppet, Chef, Saltstack and the like for simple use cases.

prun has been inspired by both  [efs2](https://github.com/madflojo/efs2) and [judo](https://github.com/rollcat/judo). Both are simple config management tools, written in golang. But both seemed to be no longer developed.

## Requirements
* pssh/parallel-ssh. 
  * Debian/Devuan, OpenSUSE, Fedora, Mageia, Slackware and Alpine Linux are providing packages. For Arch there is a package python-pssh in the AUR. There is also a port for FreeBSD.
* GNU Bash. I am not aware of any "bashisms" in the script, so other bourne compatible shells might work as well.

## Installation
```
git clone https://codeberg.org/senioradmin/prun.git
cd prun
```

**Caution** The command names of the pssh tools may be different between Linux distro version. Defaults are for Debian (`parallel-ssh` and `parallel-scp`). If they are different on your distro, edit `prun` and change the variable PSSH and PSCP at the top of the script accordingly.

```
install -m 0755 prun /usr/local/bin/prun
```

## Usage
`prun [[-h hostfile]||[-H host]] [[-s script||scriptdir]||[-c command]]`

* `-h hostfile`  hostfile is a textfile with the names of hosts (one per line)
* `-H host`  the name of a single host
* `-s script` A script (or program) which will will be executed on the remote host(s)
* `-s scriptdir` scriptdir is a directory which contains at least a script (or program) named `script`, which will be executet on the remote host(s). It may also include a script `script.local` which will be executet locally before the scriptdir is transferred to the remote hosts (e.g. for preparing config files)
* `-c command`  a single command to be executed on the remote host(s). This option exists only for convinience reasons, prun just passes this command to pssh with the `-i`parameter. You don't need prun to execute single commands in parallel, using pssh will suffice. 

## Quick Start

### Execute a single script

* Create a directory in your home dir and enter it:  `mkdir prun && cd prun`
* Create an hostfile (aka inventory) for example `inventory` : 
```
alice.example.com
bob.example.com
charlie.example.com
```

* Let's create hello.sh with a chmod +x

```
#!/bin/sh

set -eu
echo "Hello from $(hostname)!"
```

* execute with `prun -h inventory -s hello.sh`

* Ouptut shout be like this:

```
Press RETURN for running with
Script: hello.sh
Hosts:  inventory 

Hit RETURN to continue, Ctrl-C to abort

[1] 14:43:32 [SUCCESS] alice.example.com
Hello from alice.example.com !
[2] 14:43:32 [SUCCESS] bob.example.com
Hello from bob.example.com !
[3] 14:43:32 [SUCCESS] charlie.example.com
Hello from charlie.example.com !
```
### Execute scriptdir
The following example switches the nameserver on all hosts

* Inside your `prun` directory create a directory and enter it, e.g. `mkdir nameserver && cd nameserver`
* create a file named `resolv.conf` with the content
```
# a privacy respecting nameserver from dismail.de
nameserver 116.203.32.217
```

* create a file named `script`
```
#!/bin/sh

set -eu
cp -v nameserver/resolv.conf /etc/resolv.conf 
```
* Execute `chmod +x script`
* Execute `cd ..`
* Execute `prun  -h inventory -s nameserver/`
* Output:
```
Press RETURN for running with
Scriptdirectory: nameserver
Hosts: inventory 

Hit RETURN to continue, Ctrl-C to abort

[1] 15:29:00 [SUCCESS] alice.example.com
'nameserver/resolv.conf' -> '/etc/resolv.conf'
[2] 15:29:00 [SUCCESS] bob.example
'nameserver/resolv.conf' -> '/etc/resolv.conf'
[3] 15:29:00 [SUCCESS] charlie.example.com
'nameserver/resolv.conf' -> '/etc/resolv.conf'
```





